import sentry_sdk
from dynaconf import Dynaconf

settings = Dynaconf(
    envvar_prefix="SNOWPONTO",
    settings_files=["settings.yaml", ".secrets.yaml"],
    environments=True,
    load_dotenv=True,
)

# `envvar_prefix` = export envvars with `export SNOWPONTO_FOO=bar`.
# `settings_files` = Load this files in the order.

# if not settings.DEBUG:
sentry_sdk.init(settings.SENTRY_DSN, traces_sample_rate=1.0)
