import random
import string
import sys

LENGTH = int(sys.argv[1]) if len(sys.argv) == 2 and sys.argv[1].isdigit() else 48
CHAR_LIST = string.ascii_letters + string.digits + "!#$%&()*+,-./:;<=>?@[]^~"

print('"' + str().join(random.choice(CHAR_LIST) for x in range(LENGTH)) + '"')
