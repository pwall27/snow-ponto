SHELL := /bin/bash
.PHONY: all clean init install test

help:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

all: clean init test run

test:
	pytest snow_ponto/tests -v

init:
	#poetry install
	cp .template/.env .env
	cp .template/.secrets.yaml .secrets.yaml
	python .template/string_gen.py 60 >> .secrets.yaml
	
install:
	poetry install

run:
	poetry run snow-ponto

clean:
	rm -rf .cache
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info
	rm -rf htmlcov
	rm -rf .tox/
	rm -rf docs/_build
	@find . -name '*.pyc' -exec rm -rf {} \;
	@find . -name '__pycache__' -exec rm -rf {} \;
	@find . -name 'Thumbs.db' -exec rm -rf {} \;
	@find . -name '*~' -exec rm -rf {} \;
