# Snow Ponto 
Aplicação para bater ponto da turma no Ahgora via linha de comando

## Pré-requisitos
- [Python](https://www.python.org/downloads/)
- [Poetry](https://python-poetry.org/docs/#installation)


## Como Contribuir?
- Clone o repositório
```shell script
git clone git@github.com:pythrick/snow-ponto.git
```
- Acesse o diretório clonado
```shell script
cd snow-ponto
```
- Inicialize o projeto
```shell script
make init
```
- Ativar ambiente virtual
```shell script
poetry shell
```

## Configuração Inicial
O ambiente é definido no arquivo `.env`, se estiver subindo a aplicação para produção, 
defina a variável SNOWPONTO_ENV como: `SNOWPONTO_ENV=PRODUCTION`.

A configuração dos funcionários é feita no arquivo `.secrets.yaml`, substitua o conteúdo 
de exemplo da variável `TURMA` pelas credenciais reais dos funcionários.

![Editando informações dos funcionários](./media/img/tela-turma.png "Editando informações dos funcionários")

Ainda no arquivo `.secrets.yaml`, estão as variáveis: `MATRICULA_EMPRESA`, `COOKIE_IDENTITY`, `REQUEST_IDENTITY` que são 
extraídas dos `cookies` e do `body` da requisição do ponto. 

Segue abaixo um exemplo de como extrair estes dados via `curl`:

![Extraindo informações da requisição do ponto](./media/img/tela-ponto-1.png "Extraindo informações da requisição do ponto")

![Extraindo informações da requisição do ponto](./media/img/tela-curl-1.png "Extraindo informações da requisição do ponto")

## Instalando a aplicação

```shell script
$ poetry install
```

## Rodando a aplicação

```shell script
$ snow-ponto
```


## Autor
- Patrick Rodrigues
