import typer

from snow_ponto import services


app = typer.Typer()


@app.command("bater-pontos")
def bater_pontos():
    try:
        for resultado in services.bater_ponto_da_turma():
            if resultado["sucesso"]:
                typer.echo(
                    f"{resultado['timestamp']} - {resultado['nome']} bateu ponto com sucesso."
                )
            else:
                typer.echo(
                    f"{resultado['timestamp']} - {resultado['nome']} não conseguiu bater o ponto. "
                    f"Detalhes: {resultado['detalhes']}"
                )
    except Exception as e:
        typer.echo(e)
        raise typer.Abort()


def main():
    app()


if __name__ == "__main__":
    app()
