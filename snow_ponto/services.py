import time
from datetime import datetime
from typing import Dict, List, Union

import httpx
from sentry_sdk import capture_exception, configure_scope

from config import settings
from snow_ponto.exceptions import BaterPontoError

_AHGORA_API_HEADERS = {
    "authority": "www.ahgora.com.br",
    "accept": "*/*",
    "dnt": "1",
    "x-requested-with": "XMLHttpRequest",
    "user-agent": (
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) "
        "AppleWebKit/537.36 (KHTML, like Gecko) "
        "Chrome/84.0.4147.135 "
        "Safari/537.36"
    ),
    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
    "origin": "https://www.ahgora.com.br",
    "sec-fetch-site": "same-origin",
    "sec-fetch-mode": "cors",
    "sec-fetch-dest": "empty",
    "referer": "https://www.ahgora.com.br/batidaonline",
    "accept-language": "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7",
}

_AHGORA_API_COOKIES = {
    "company": settings.MATRICULA_EMPRESA,
    "identity": settings.COOKIE_IDENTITY,
    "origin": "pw2",
    "key": "",
}

FERIADOS = [datetime.strptime(f, "%Y-%m-%d").date() for f in settings.FERIADOS]


def bater_ponto(matricula: str, password: str, tentativa=1):
    data = {
        "account": matricula,
        "password": password,
        "identity": settings.REQUEST_IDENTITY,
        "origin": "pw2",
        "key": "",
    }

    try:
        resposta = httpx.post(
            settings.AHGORA_API_URL,
            data=data,
            headers=_AHGORA_API_HEADERS,
            cookies=_AHGORA_API_COOKIES,
        )
        resposta.raise_for_status()
    except Exception as e:
        if tentativa > settings.TENTATIVAS:
            raise e
        print(
            f"Falha ao bater ponto com a matrícula: {matricula}. "
            f"Tentativa: {tentativa}/{settings.TENTATIVAS}. "
            f"Detalhes: {e}"
        )
        time.sleep(tentativa)
        return bater_ponto(matricula, password, tentativa=tentativa + 1)

    resultado = resposta.json()
    if not resultado.get("result"):
        raise BaterPontoError(resultado.get("reason"))


def bater_ponto_da_turma() -> List[Dict[str, Union[str, bool]]]:
    if datetime.today().date() in FERIADOS:
        print(f"Feriado: {datetime.today().date()}")
        return []
    resultados = []
    for membro in settings.TURMA:
        try:
            bater_ponto(membro["matricula"], membro["password"])
            time.sleep(0.5)
            resultados.append(
                {
                    "nome": membro["nome"],
                    "sucesso": True,
                    "detalhes": "",
                    "timestamp": datetime.now().isoformat(),
                }
            )
        except Exception as e:
            with configure_scope() as scope:
                scope.set_tag("matricula", membro["matricula"])
                scope.set_tag("nome", membro["nome"])
            capture_exception(e)

            resultados.append(
                {
                    "nome": membro["nome"],
                    "sucesso": False,
                    "detalhes": str(e),
                    "timestamp": datetime.now().isoformat(),
                }
            )
    return resultados
